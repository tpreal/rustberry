// Copyright (C) 2018  Adam Gausmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Constants transcribed from the reference [BCM2835 ARM Peripherals].
//! 
//! # Conventions
//!
//! - All page offsets are relative to the peripheral offset which varies
//! depending on the board and operating system (if any).
//! - All register locations are relative to their respective page offset.
//! - Bitflags for each register are provided as their own bitmasks.
//! - Fields with multiple bits have two constants, `_SHIFT` and `_MASK`.
//! Shift defines how far the start of the data is from the beginning of the
//! word (LSB-side) in bits, and mask is a bitmask to constrain the value to
//! the correct number of bits (once it has been shifted into its register
//! position) to prevent overflow.
//! - Multiple bit fields at the beginning of a register will still have a
//! shift constant for consistency; however, it may be omitted in cases where
//! it is the only field in the register. The mask may also be omitted if the
//! field is allocated to the entire register.
//! - Multiple bit fields with multiple enumerated options instead of numeric
//! values have constants defining those options instead of
//! a shift value. (Some exceptions apply; for example, fields with multiple
//! locations like `GPIO_FSEL` may still have arrays containing shift values
//! for each location.)
//!
//! # Arrays
//!
//! In some places where there are many of the same type of item with the same
//! controls (example: GPIO pins), the common controls (whether a field or a
//! register) have been put together into an array so they may be indexed by a
//! numeric ID. If there is no clear numeric ID to assign to each item
//! (example: clocks), then constants are provided to assign those IDs.
//!
//! [BCM2835 ARM Peripherals]: https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf

// As long as it's complete and up to date...
#![allow(dead_code)]

pub const CM_CLOCKS: usize = 4;

/// The number of pins supported by the platform.
/// Valid pin numbers are within the range `0..GPIO_PINS`.
pub const GP_PINS: usize = 54;

/// The number of PWM channels supported by the platform.
/// Valid channel numbers are within the range `0..PWM_CHANNELS`.
pub const PWM_CHANNELS: usize = 2;

pub const AUX_SPI: usize = 2;

pub const CM_CLOCK_GP0: usize = 0;
pub const CM_CLOCK_GP1: usize = 1;
pub const CM_CLOCK_GP2: usize = 2;
pub const CM_CLOCK_PWM: usize = 3;

pub const PAGE_SIZE: usize = 0x1000;

pub const TIMER_PAGE_OFFSET: usize = 0x00_3000;
pub const CM_PAGE_OFFSET:    usize = 0x10_1000;
pub const GP_PAGE_OFFSET:    usize = 0x20_0000;
pub const UART_PAGE_OFFSET:  usize = 0x20_1000;
pub const SPI_PAGE_OFFSET:   usize = 0x20_4000;
pub const BSC0_PAGE_OFFSET:  usize = 0x20_5000;
pub const PWM_PAGE_OFFSET:   usize = 0x20_C000;
pub const SLV_PAGE_OFFSET:   usize = 0x21_4000;
pub const AUX_PAGE_OFFSET:   usize = 0x21_5000;
pub const BSC1_PAGE_OFFSET:  usize = 0x80_4000;

pub const TIMER_CS:  usize = 0x00;
pub const TIMER_CLO: usize = 0x04;
pub const TIMER_CHI: usize = 0x08;
pub const TIMER_C0:  usize = 0x0c;
pub const TIMER_C1:  usize = 0x10;
pub const TIMER_C2:  usize = 0x14;
pub const TIMER_C3:  usize = 0x18;

pub const CM_GP0CTL: usize = 0x70;
pub const CM_GP0DIV: usize = 0x74;
pub const CM_GP1CTL: usize = 0x78;
pub const CM_GP1DIV: usize = 0x7c;
pub const CM_GP2CTL: usize = 0x80;
pub const CM_GP2DIV: usize = 0x84;
pub const CM_PWMCTL: usize = 0xa0;
pub const CM_PWMDIV: usize = 0xa4;

pub const CM_CTL: [usize; CM_CLOCKS] = [
    CM_GP0CTL, CM_GP1CTL, CM_GP2CTL, CM_PWMCTL,
];

pub const CM_DIV: [usize; CM_CLOCKS] = [
    CM_GP0DIV, CM_GP1DIV, CM_GP2DIV, CM_PWMDIV,
];

pub const GP_FSEL0:   usize = 0x00;
pub const GP_FSEL1:   usize = 0x04;
pub const GP_FSEL2:   usize = 0x08;
pub const GP_FSEL3:   usize = 0x0c;
pub const GP_FSEL4:   usize = 0x10;
pub const GP_FSEL5:   usize = 0x14;
pub const GP_SET0:    usize = 0x1c;
pub const GP_SET1:    usize = 0x20;
pub const GP_CLR0:    usize = 0x28;
pub const GP_CLR1:    usize = 0x2c;
pub const GP_LEV0:    usize = 0x34;
pub const GP_LEV1:    usize = 0x38;
pub const GP_EDS0:    usize = 0x40;
pub const GP_EDS1:    usize = 0x44;
pub const GP_REN0:    usize = 0x4c;
pub const GP_REN1:    usize = 0x50;
pub const GP_FEN0:    usize = 0x58;
pub const GP_FEN1:    usize = 0x5c;
pub const GP_HEN0:    usize = 0x64;
pub const GP_HEN1:    usize = 0x68;
pub const GP_LEN0:    usize = 0x70;
pub const GP_LEN1:    usize = 0x74;
pub const GP_AREN0:   usize = 0x7c;
pub const GP_AREN1:   usize = 0x80;
pub const GP_AFEN0:   usize = 0x88;
pub const GP_AFEN1:   usize = 0x8c;
pub const GP_PUD:     usize = 0x94;
pub const GP_PUDCLK0: usize = 0x98;
pub const GP_PUDCLK1: usize = 0x9c;

pub const GP_FSEL: [usize; GP_PINS] = [
    GP_FSEL0, GP_FSEL0, GP_FSEL0, GP_FSEL0, GP_FSEL0,
    GP_FSEL0, GP_FSEL0, GP_FSEL0, GP_FSEL0, GP_FSEL0,
    GP_FSEL1, GP_FSEL1, GP_FSEL1, GP_FSEL1, GP_FSEL1,
    GP_FSEL1, GP_FSEL1, GP_FSEL1, GP_FSEL1, GP_FSEL1,
    GP_FSEL2, GP_FSEL2, GP_FSEL2, GP_FSEL2, GP_FSEL2,
    GP_FSEL2, GP_FSEL2, GP_FSEL2, GP_FSEL2, GP_FSEL2,
    GP_FSEL3, GP_FSEL3, GP_FSEL3, GP_FSEL3, GP_FSEL3,
    GP_FSEL3, GP_FSEL3, GP_FSEL3, GP_FSEL3, GP_FSEL3,
    GP_FSEL4, GP_FSEL4, GP_FSEL4, GP_FSEL4, GP_FSEL4,
    GP_FSEL4, GP_FSEL4, GP_FSEL4, GP_FSEL4, GP_FSEL4,
    GP_FSEL5, GP_FSEL5, GP_FSEL5, GP_FSEL5,
];

pub const GP_SET: [usize; GP_PINS] = [
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET0, GP_SET0, GP_SET0, GP_SET0,
    GP_SET1, GP_SET1, GP_SET1, GP_SET1,
    GP_SET1, GP_SET1, GP_SET1, GP_SET1,
    GP_SET1, GP_SET1, GP_SET1, GP_SET1,
    GP_SET1, GP_SET1, GP_SET1, GP_SET1,
    GP_SET1, GP_SET1, GP_SET1, GP_SET1,
    GP_SET1, GP_SET1,
];

pub const GP_CLR: [usize; GP_PINS] = [
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR0, GP_CLR0, GP_CLR0, GP_CLR0,
    GP_CLR1, GP_CLR1, GP_CLR1, GP_CLR1,
    GP_CLR1, GP_CLR1, GP_CLR1, GP_CLR1,
    GP_CLR1, GP_CLR1, GP_CLR1, GP_CLR1,
    GP_CLR1, GP_CLR1, GP_CLR1, GP_CLR1,
    GP_CLR1, GP_CLR1, GP_CLR1, GP_CLR1,
    GP_CLR1, GP_CLR1,
];

pub const GP_LEV: [usize; GP_PINS] = [
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV0, GP_LEV0, GP_LEV0, GP_LEV0,
    GP_LEV1, GP_LEV1, GP_LEV1, GP_LEV1,
    GP_LEV1, GP_LEV1, GP_LEV1, GP_LEV1,
    GP_LEV1, GP_LEV1, GP_LEV1, GP_LEV1,
    GP_LEV1, GP_LEV1, GP_LEV1, GP_LEV1,
    GP_LEV1, GP_LEV1, GP_LEV1, GP_LEV1,
    GP_LEV1, GP_LEV1,
];

pub const GP_EDS: [usize; GP_PINS] = [
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS0, GP_EDS0, GP_EDS0, GP_EDS0,
    GP_EDS1, GP_EDS1, GP_EDS1, GP_EDS1,
    GP_EDS1, GP_EDS1, GP_EDS1, GP_EDS1,
    GP_EDS1, GP_EDS1, GP_EDS1, GP_EDS1,
    GP_EDS1, GP_EDS1, GP_EDS1, GP_EDS1,
    GP_EDS1, GP_EDS1, GP_EDS1, GP_EDS1,
    GP_EDS1, GP_EDS1,
];

pub const GP_REN: [usize; GP_PINS] = [
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN0, GP_REN0, GP_REN0, GP_REN0,
    GP_REN1, GP_REN1, GP_REN1, GP_REN1,
    GP_REN1, GP_REN1, GP_REN1, GP_REN1,
    GP_REN1, GP_REN1, GP_REN1, GP_REN1,
    GP_REN1, GP_REN1, GP_REN1, GP_REN1,
    GP_REN1, GP_REN1, GP_REN1, GP_REN1,
    GP_REN1, GP_REN1,
];

pub const GP_FEN: [usize; GP_PINS] = [
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN0, GP_FEN0, GP_FEN0, GP_FEN0,
    GP_FEN1, GP_FEN1, GP_FEN1, GP_FEN1,
    GP_FEN1, GP_FEN1, GP_FEN1, GP_FEN1,
    GP_FEN1, GP_FEN1, GP_FEN1, GP_FEN1,
    GP_FEN1, GP_FEN1, GP_FEN1, GP_FEN1,
    GP_FEN1, GP_FEN1, GP_FEN1, GP_FEN1,
    GP_FEN1, GP_FEN1,
];

pub const GP_HEN: [usize; GP_PINS] = [
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN0, GP_HEN0, GP_HEN0, GP_HEN0,
    GP_HEN1, GP_HEN1, GP_HEN1, GP_HEN1,
    GP_HEN1, GP_HEN1, GP_HEN1, GP_HEN1,
    GP_HEN1, GP_HEN1, GP_HEN1, GP_HEN1,
    GP_HEN1, GP_HEN1, GP_HEN1, GP_HEN1,
    GP_HEN1, GP_HEN1, GP_HEN1, GP_HEN1,
    GP_HEN1, GP_HEN1,
];

pub const GP_LEN: [usize; GP_PINS] = [
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN0, GP_LEN0, GP_LEN0, GP_LEN0,
    GP_LEN1, GP_LEN1, GP_LEN1, GP_LEN1,
    GP_LEN1, GP_LEN1, GP_LEN1, GP_LEN1,
    GP_LEN1, GP_LEN1, GP_LEN1, GP_LEN1,
    GP_LEN1, GP_LEN1, GP_LEN1, GP_LEN1,
    GP_LEN1, GP_LEN1, GP_LEN1, GP_LEN1,
    GP_LEN1, GP_LEN1,
];

pub const GP_AREN: [usize; GP_PINS] = [
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN0, GP_AREN0, GP_AREN0, GP_AREN0,
    GP_AREN1, GP_AREN1, GP_AREN1, GP_AREN1,
    GP_AREN1, GP_AREN1, GP_AREN1, GP_AREN1,
    GP_AREN1, GP_AREN1, GP_AREN1, GP_AREN1,
    GP_AREN1, GP_AREN1, GP_AREN1, GP_AREN1,
    GP_AREN1, GP_AREN1, GP_AREN1, GP_AREN1,
    GP_AREN1, GP_AREN1,
];

pub const GP_AFEN: [usize; GP_PINS] = [
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN0, GP_AFEN0, GP_AFEN0, GP_AFEN0,
    GP_AFEN1, GP_AFEN1, GP_AFEN1, GP_AFEN1,
    GP_AFEN1, GP_AFEN1, GP_AFEN1, GP_AFEN1,
    GP_AFEN1, GP_AFEN1, GP_AFEN1, GP_AFEN1,
    GP_AFEN1, GP_AFEN1, GP_AFEN1, GP_AFEN1,
    GP_AFEN1, GP_AFEN1, GP_AFEN1, GP_AFEN1,
    GP_AFEN1, GP_AFEN1,
];

pub const GP_PUDCLK: [usize; GP_PINS] = [
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0, GP_PUDCLK0,
    GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1,
    GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1,
    GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1,
    GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1,
    GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1, GP_PUDCLK1,
    GP_PUDCLK1, GP_PUDCLK1,
];

pub const UART_DR:     usize = 0x00;
pub const UART_RSRECR: usize = 0x04;
pub const UART_FR:     usize = 0x18;
pub const UART_IBRD:   usize = 0x24;
pub const UART_FBRD:   usize = 0x28;
pub const UART_LCRH:   usize = 0x2c;
pub const UART_CR:     usize = 0x30;
pub const UART_IFLS:   usize = 0x34;
pub const UART_IMSC:   usize = 0x38;
pub const UART_RIS:    usize = 0x3c;
pub const UART_MIS:    usize = 0x40;
pub const UART_ICR:    usize = 0x44;
pub const UART_ITCR:   usize = 0x80;
pub const UART_ITIP:   usize = 0x84;
pub const UART_ITOP:   usize = 0x88;
pub const UART_TDR:    usize = 0x8c;

pub const SPI_CS:   usize = 0x00;
pub const SPI_FIFO: usize = 0x04;
pub const SPI_CLK:  usize = 0x08;
pub const SPI_DLEN: usize = 0x0c;
pub const SPI_LTOH: usize = 0x10;
pub const SPI_DC:   usize = 0x14;

pub const BSC_C:    usize = 0x00;
pub const BSC_S:    usize = 0x04;
pub const BSC_DLEN: usize = 0x08;
pub const BSC_A:    usize = 0x0c;
pub const BSC_FIFO: usize = 0x10;
pub const BSC_DIV:  usize = 0x14;
pub const BSC_DEL:  usize = 0x18;
pub const BSC_CLKT: usize = 0x1c;

pub const PWM_CTL:  usize = 0x00;
pub const PWM_STA:  usize = 0x04;
pub const PWM_DMAC: usize = 0x08;
pub const PWM_RNG1: usize = 0x10;
pub const PWM_DAT1: usize = 0x14;
pub const PWM_FIF1: usize = 0x18;
pub const PWM_RNG2: usize = 0x20;
pub const PWM_DAT2: usize = 0x24;

pub const PWM_RNG: [usize; PWM_CHANNELS] = [
    PWM_RNG1, PWM_RNG2,
];

pub const PWM_DAT: [usize; PWM_CHANNELS] = [
    PWM_DAT1, PWM_DAT2,
];

pub const SLV_DR:  usize = 0x00;
pub const SLV_RSR: usize = 0x04;
pub const SLV_SLV: usize = 0x08;
pub const SLV_CR:  usize = 0x0c;
pub const SLV_FR:  usize = 0x10;
pub const SLV_IFLS: usize = 0x14;
pub const SLV_IMSC: usize = 0x18;
pub const SLV_RIS:  usize = 0x1c;
pub const SLV_MIS:  usize = 0x20;
pub const SLV_ICR:  usize = 0x24;
pub const SLV_DMACR: usize = 0x28;
pub const SLV_TDR:  usize = 0x2c;
pub const SLV_GPUSTAT: usize = 0x30;
pub const SLV_HCTRL: usize = 0x34;
pub const SLV_DEBUG1: usize = 0x38;
pub const SLV_DEBUG2: usize = 0x3c;

pub const AUX_IRQ:        usize = 0x00;
pub const AUX_ENB:        usize = 0x04;
pub const AUX_MU_IO:      usize = 0x40;
pub const AUX_MU_IER:     usize = 0x44;
pub const AUX_MU_IIR:     usize = 0x48;
pub const AUX_MU_LCR:     usize = 0x4c;
pub const AUX_MU_MCR:     usize = 0x50;
pub const AUX_MU_LSR:     usize = 0x54;
pub const AUX_MU_MSR:     usize = 0x58;
pub const AUX_MU_SCRATCH: usize = 0x5c;
pub const AUX_MU_CNTL:    usize = 0x60;
pub const AUX_MU_STAT:    usize = 0x64;
pub const AUX_MU_BAUD:    usize = 0x68;
pub const AUX_SPI1_CNTL0: usize = 0x80;
pub const AUX_SPI1_CNTL1: usize = 0x84;
pub const AUX_SPI1_STAT:  usize = 0x88;
pub const AUX_SPI1_IO:    usize = 0x90;
pub const AUX_SPI1_PEEK:  usize = 0x94;
pub const AUX_SPI2_CNTL0: usize = 0xc0;
pub const AUX_SPI2_CNTL1: usize = 0xc4;
pub const AUX_SPI2_STAT:  usize = 0xc8;
pub const AUX_SPI2_IO:    usize = 0xd0;
pub const AUX_SPI2_PEEK:  usize = 0xd4;

pub const AUX_SPI_CNTL0: [usize; AUX_SPI] = [
    AUX_SPI1_CNTL0, AUX_SPI2_CNTL0,
];

pub const AUX_SPI_CNTL1: [usize; AUX_SPI] = [
    AUX_SPI1_CNTL1, AUX_SPI2_CNTL1,
];

pub const AUX_SPI_STAT: [usize; AUX_SPI] = [
    AUX_SPI1_STAT, AUX_SPI2_STAT,
];

pub const AUX_SPI_IO: [usize; AUX_SPI] = [
    AUX_SPI1_IO, AUX_SPI2_IO,
];

pub const AUX_SPI_PEEK: [usize; AUX_SPI] = [
    AUX_SPI1_PEEK, AUX_SPI2_PEEK,
];

pub const TIMER_CS_M0: u32 = 0x00000001;
pub const TIMER_CS_M1: u32 = 0x00000002;
pub const TIMER_CS_M2: u32 = 0x00000004;
pub const TIMER_CS_M3: u32 = 0x00000008;

pub const CM_CTL_SRC_MASK:     u32 = 0x0000000f;
pub const CM_CTL_SRC_GND:      u32 = 0x00000000;
pub const CM_CTL_SRC_OSC:      u32 = 0x00000001;
pub const CM_CTL_SRC_PLLA_PER: u32 = 0x00000004;
pub const CM_CTL_SRC_PLLC_PER: u32 = 0x00000005;
pub const CM_CTL_SRC_PLLD_PER: u32 = 0x00000006;
pub const CM_CTL_SRC_HDMI_AUX: u32 = 0x00000007;
pub const CM_CTL_ENAB:         u32 = 0x00000010;
pub const CM_CTL_KILL:         u32 = 0x00000020;
pub const CM_CTL_BUSY:         u32 = 0x00000080;
pub const CM_CTL_FLIP:         u32 = 0x00000100;
pub const CM_CTL_MASH_SHIFT:    u8 = 9;
pub const CM_CTL_MASH_MASK:    u32 = 0x00000600;
pub const CM_CTL_PASSWD:       u32 = 0x5a000000;

pub const CM_DIV_DIVF_SHIFT: u8 = 0;
pub const CM_DIV_DIVF_MASK: u32 = 0x00000fff;
pub const CM_DIV_DIVI_SHIFT: u8 = 12;
pub const CM_DIV_DIVI_MASK: u32 = 0x00fff000;
pub const CM_DIV_PASSWD:    u32 = 0x5a000000;

pub const GP_PIN: [u32; GP_PINS] = [
    0x00000001, 0x00000002, 0x00000004, 0x00000008,
    0x00000010, 0x00000020, 0x00000040, 0x00000080,
    0x00000100, 0x00000200, 0x00000400, 0x00000800,
    0x00001000, 0x00002000, 0x00004000, 0x00008000,
    0x00010000, 0x00020000, 0x00040000, 0x00080000,
    0x00100000, 0x00200000, 0x00400000, 0x00800000,
    0x01000000, 0x02000000, 0x04000000, 0x08000000,
    0x10000000, 0x20000000, 0x40000000, 0x80000000,
    0x00000001, 0x00000002, 0x00000004, 0x00000008,
    0x00000010, 0x00000020, 0x00000040, 0x00000080,
    0x00000100, 0x00000200, 0x00000400, 0x00000800,
    0x00001000, 0x00002000, 0x00004000, 0x00008000,
    0x00010000, 0x00020000, 0x00040000, 0x00080000,
    0x00100000, 0x00200000,
];

pub const GP_FSEL_SHIFT: [u8; GP_PINS] = [
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27,
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27,
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27,
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27,
    0, 3, 6, 9, 12, 15, 18, 21, 24, 27,
    0, 3, 6, 9,
];

pub const GP_FSEL_MASK: [u32; GP_PINS] = [
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00, 0x00007000,
    0x00038000, 0x001c0000, 0x00e00000, 0x07000000, 0x38000000,
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00, 0x00007000,
    0x00038000, 0x001c0000, 0x00e00000, 0x07000000, 0x38000000,
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00, 0x00007000,
    0x00038000, 0x001c0000, 0x00e00000, 0x07000000, 0x38000000,
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00, 0x00007000,
    0x00038000, 0x001c0000, 0x00e00000, 0x07000000, 0x38000000,
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00, 0x00007000,
    0x00038000, 0x001c0000, 0x00e00000, 0x07000000, 0x38000000,
    0x00000007, 0x00000038, 0x000001c0, 0x00000e00,
];

pub const GP_FSEL_IN:   u32 = 0b000;
pub const GP_FSEL_OUT:  u32 = 0b001;
pub const GP_FSEL_ALT0: u32 = 0b100;
pub const GP_FSEL_ALT1: u32 = 0b101;
pub const GP_FSEL_ALT2: u32 = 0b110;
pub const GP_FSEL_ALT3: u32 = 0b111;
pub const GP_FSEL_ALT4: u32 = 0b011;
pub const GP_FSEL_ALT5: u32 = 0b010;

pub const GP_PUD_MASK: u32 = 0x00000003;
pub const GP_PUD_OFF:  u32 = 0x00000000;
pub const GP_PUD_DOWN: u32 = 0x00000001;
pub const GP_PUD_UP:   u32 = 0x00000002;

pub const UART_DR_DATA_SHIFT: u8 = 0;
pub const UART_DR_DATA_MASK: u32 = 0x000000ff;
pub const UART_DR_FE:        u32 = 0x00000100;
pub const UART_DR_PE:        u32 = 0x00000200;
pub const UART_DR_BE:        u32 = 0x00000400;
pub const UART_DR_OE:        u32 = 0x00000800;

pub const UART_RSRECR_FE: u32 = 0x00000001;
pub const UART_RSRECR_PE: u32 = 0x00000002;
pub const UART_RSRECR_BE: u32 = 0x00000004;
pub const UART_RSRECR_OE: u32 = 0x00000008;

pub const UART_FR_CTS:  u32 = 0x00000001;
pub const UART_FR_BUSY: u32 = 0x00000008;
pub const UART_FR_RXFE: u32 = 0x00000010;
pub const UART_FR_TXFF: u32 = 0x00000020;
pub const UART_FR_RXFF: u32 = 0x00000040;
pub const UART_FR_TXFE: u32 = 0x00000080;

pub const UART_IBRD_MASK: u32 = 0x0000ffff;

pub const UART_FBRD_MASK: u32 = 0x0000003f;

pub const UART_LCRH_BRK:       u32 = 0x00000001;
pub const UART_LCRH_PEN:       u32 = 0x00000002;
pub const UART_LCRH_EPS:       u32 = 0x00000004;
pub const UART_LCRH_STP2:      u32 = 0x00000008;
pub const UART_LCRH_FEN:       u32 = 0x00000010;
pub const UART_LCRH_WLEN_MASK: u32 = 0x00000060;
pub const UART_LCRH_WLEN5:     u32 = 0x00000000;
pub const UART_LCRH_WLEN6:     u32 = 0x00000020;
pub const UART_LCRH_WLEN7:     u32 = 0x00000040;
pub const UART_LCRH_WLEN8:     u32 = 0x00000060;
pub const UART_LCRH_SPS:       u32 = 0x00000080;

pub const UART_CR_UARTEN: u32 = 0x00000001;
pub const UART_CR_LBE:    u32 = 0x00000080;
pub const UART_CR_TXE:    u32 = 0x00000100;
pub const UART_CR_RXE:    u32 = 0x00000200;
pub const UART_CR_RTS:    u32 = 0x00000800;
pub const UART_CR_RTSEN:  u32 = 0x00004000;
pub const UART_CR_CTSEN:  u32 = 0x00008000;

pub const UART_IFLS_TX_MASK: u32 = 0x00000007;
pub const UART_IFLS_TX_1_8:  u32 = 0x00000000;
pub const UART_IFLS_TX_1_4:  u32 = 0x00000001;
pub const UART_IFLS_TX_1_2:  u32 = 0x00000002;
pub const UART_IFLS_TX_3_4:  u32 = 0x00000003;
pub const UART_IFLS_TX_7_8:  u32 = 0x00000004;
pub const UART_IFLS_RX_MASK: u32 = 0x00000038;
pub const UART_IFLS_RX_1_8:  u32 = 0x00000000;
pub const UART_IFLS_RX_1_4:  u32 = 0x00000008;
pub const UART_IFLS_RX_1_2:  u32 = 0x00000010;
pub const UART_IFLS_RX_3_4:  u32 = 0x00000018;
pub const UART_IFLS_RX_7_8:  u32 = 0x00000020;

pub const UART_IR_CTS: u32 = 0x00000002;
pub const UART_IR_RX:  u32 = 0x00000010;
pub const UART_IR_TX:  u32 = 0x00000020;
pub const UART_IR_RT:  u32 = 0x00000040;
pub const UART_IR_FE:  u32 = 0x00000080;
pub const UART_IR_PE:  u32 = 0x00000100;
pub const UART_IR_BE:  u32 = 0x00000200;
pub const UART_IR_OE:  u32 = 0x00000400;

pub const UART_ITCR_ITCR0: u32 = 0x00000001;
pub const UART_ITCR_ITCR1: u32 = 0x00000002;

pub const UART_ITIP_ITIP0: u32 = 0x00000001;
pub const UART_ITIP_ITIP3: u32 = 0x00000008;

pub const UART_ITOP_ITOP0:  u32 = 0x00000001;
pub const UART_ITOP_ITOP3:  u32 = 0x00000008;
pub const UART_ITOP_ITOP6:  u32 = 0x00000040;
pub const UART_ITOP_ITOP7:  u32 = 0x00000080;
pub const UART_ITOP_ITOP8:  u32 = 0x00000100;
pub const UART_ITOP_ITOP9:  u32 = 0x00000200;
pub const UART_ITOP_ITOP10: u32 = 0x00000400;
pub const UART_ITOP_ITOP11: u32 = 0x00000800;

pub const UART_TDR_MASK: u32 = 0x000007ff;

pub const SPI_CS_CS_MASK:  u32 = 0x00000003;
pub const SPI_CS_CS0:      u32 = 0x00000000;
pub const SPI_CS_CS1:      u32 = 0x00000001;
pub const SPI_CS_CS2:      u32 = 0x00000002;
pub const SPI_CS_CPHA:     u32 = 0x00000004;
pub const SPI_CS_CPOL:     u32 = 0x00000008;
pub const SPI_CS_CLEAR_TX: u32 = 0x00000010;
pub const SPI_CS_CLEAR_RX: u32 = 0x00000020;
pub const SPI_CS_CSPOL:    u32 = 0x00000040;
pub const SPI_CS_TA:       u32 = 0x00000080;
pub const SPI_CS_DMAEN:    u32 = 0x00000100;
pub const SPI_CS_INTD:     u32 = 0x00000200;
pub const SPI_CS_INTR:     u32 = 0x00000400;
pub const SPI_CS_ADCS:     u32 = 0x00000800;
pub const SPI_CS_REN:      u32 = 0x00001000;
pub const SPI_CS_LEN:      u32 = 0x00002000;
pub const SPI_CS_DONE:     u32 = 0x00010000;
pub const SPI_CS_RXD:      u32 = 0x00020000;
pub const SPI_CS_TXD:      u32 = 0x00040000;
pub const SPI_CS_RXR:      u32 = 0x00080000;
pub const SPI_CS_RXF:      u32 = 0x00100000;
pub const SPI_CS_CSPOL0:   u32 = 0x00200000;
pub const SPI_CS_CSPOL1:   u32 = 0x00400000;
pub const SPI_CS_CSPOL2:   u32 = 0x00800000;
pub const SPI_CS_DMA_LEN:  u32 = 0x01000000;
pub const SPI_CS_LEN_LONG: u32 = 0x02000000;

pub const SPI_CLK_CDIV_SHIFT: u8 = 0;
pub const SPI_CLK_CDIV_MASK: u32 = 0x0000ffff;

pub const SPI_DLEN_LEN_SHIFT: u8 = 0;
pub const SPI_DLEN_LEN_MASK: u32 = 0x0000ffff;

pub const SPI_LTOH_TOH_SHIFT: u8 = 0;
pub const SPI_LTOH_TOH_MASK: u32 = 0x00000007;

pub const SPI_DC_TDREQ_SHIFT:  u8 = 0;
pub const SPI_DC_TDREQ_MASK:  u32 = 0x000000ff;
pub const SPI_DC_TPANIC_SHIFT: u8 = 8;
pub const SPI_DC_TPANIC_MASK: u32 = 0x0000ff00;
pub const SPI_DC_RDREQ_SHIFT:  u8 = 16;
pub const SPI_DC_RDREQ_MASK:  u32 = 0x00ff0000;
pub const SPI_DC_RPANIC_SHIFT: u8 = 24;
pub const SPI_DC_RPANIC_MASK: u32 = 0xff000000;

pub const BSC_C_READ:  u32 = 0x00000001;
pub const BSC_C_CLEAR: u32 = 0x00000030;
pub const BSC_C_ST:    u32 = 0x00000080;
pub const BSC_C_INTD:  u32 = 0x00000100;
pub const BSC_C_INTT:  u32 = 0x00000200;
pub const BSC_C_INTR:  u32 = 0x00000400;
pub const BSC_C_I2CEN: u32 = 0x00008000;

pub const BSC_S_TA:   u32 = 0x00000001;
pub const BSC_S_DONE: u32 = 0x00000002;
pub const BSC_S_TXW:  u32 = 0x00000004;
pub const BSC_S_RXR:  u32 = 0x00000008;
pub const BSC_S_TXD:  u32 = 0x00000010;
pub const BSC_S_RXD:  u32 = 0x00000020;
pub const BSC_S_TXE:  u32 = 0x00000040;
pub const BSC_S_RXF:  u32 = 0x00000080;
pub const BSC_S_ERR:  u32 = 0x00000100;
pub const BSC_S_CLKT: u32 = 0x00000200;

pub const BSC_DLEN_MASK: u32 = 0x0000ffff;

pub const BSC_A_MASK: u32 = 0x0000007f;

pub const BSC_FIFO_MASK: u32 = 0x000000ff;

pub const BSC_DIV_MASK: u32 = 0x0000ffff;

pub const BSC_DEL_REDL_SHIFT: u8 = 0;
pub const BSC_DEL_REDL_MASK: u32 = 0x0000ffff;
pub const BSC_DEL_FEDL_SHIFT: u8 = 16;
pub const BSC_DEL_FEDL_MASK: u32 = 0x0000ffff;

pub const BSC_CLKT_MASK: u32 = 0x0000ffff;

pub const PWM_CTL_PWEN1: u32 = 0x00000001;
pub const PWM_CTL_MODE1: u32 = 0x00000002;
pub const PWM_CTL_RPTL1: u32 = 0x00000004;
pub const PWM_CTL_SBIT1: u32 = 0x00000008;
pub const PWM_CTL_POLA1: u32 = 0x00000010;
pub const PWM_CTL_USEF1: u32 = 0x00000020;
pub const PWM_CTL_CLRF1: u32 = 0x00000040;
pub const PWM_CTL_MSEN1: u32 = 0x00000080;
pub const PWM_CTL_PWEN2: u32 = 0x00000100;
pub const PWM_CTL_MODE2: u32 = 0x00000200;
pub const PWM_CTL_RPTL2: u32 = 0x00000400;
pub const PWM_CTL_SBIT2: u32 = 0x00000800;
pub const PWM_CTL_POLA2: u32 = 0x00001000;
pub const PWM_CTL_USEF2: u32 = 0x00002000;
pub const PWM_CTL_MSEN2: u32 = 0x00008000;

pub const PWM_STA_FULL1: u32 = 0x00000001;
pub const PWM_STA_EMPT1: u32 = 0x00000002;
pub const PWM_STA_WERR1: u32 = 0x00000004;
pub const PWM_STA_RERR1: u32 = 0x00000008;
pub const PWM_STA_GAPO1: u32 = 0x00000010;
pub const PWM_STA_GAPO2: u32 = 0x00000020;
pub const PWM_STA_GAPO3: u32 = 0x00000040;
pub const PWM_STA_GAPO4: u32 = 0x00000080;
pub const PWM_STA_BERR:  u32 = 0x00000100;
pub const PWM_STA_STA1:  u32 = 0x00000200;
pub const PWM_STA_STA2:  u32 = 0x00000400;
pub const PWM_STA_STA3:  u32 = 0x00000800;
pub const PWM_STA_STA4:  u32 = 0x00001000;

pub const PWM_DMA_DREQ_SHIFT:  u8 = 0;
pub const PWM_DMA_PANIC_SHIFT: u8 = 8;
pub const PWM_DMA_DREQ_MASK:  u32 = 0x000000ff;
pub const PWM_DMA_PANIC_MASK: u32 = 0x0000ff00;
pub const PWM_DMA_ENAB:       u32 = 0x10000000;

pub const PWM_CTL_PWEN: [u32; PWM_CHANNELS] = [
    PWM_CTL_PWEN1, PWM_CTL_PWEN2,
];

pub const PWM_CTL_MODE: [u32; PWM_CHANNELS] = [
    PWM_CTL_MODE1, PWM_CTL_MODE2,
];

pub const PWM_CTL_RPTL: [u32; PWM_CHANNELS] = [
    PWM_CTL_RPTL1, PWM_CTL_RPTL2,
];

pub const PWM_CTL_SBIT: [u32; PWM_CHANNELS] = [
    PWM_CTL_SBIT1, PWM_CTL_SBIT2,
];

pub const PWM_CTL_POLA: [u32; PWM_CHANNELS] = [
    PWM_CTL_POLA1, PWM_CTL_POLA2,
];

pub const PWM_CTL_USEF: [u32; PWM_CHANNELS] = [
    PWM_CTL_USEF1, PWM_CTL_USEF2,
];

pub const PWM_CTL_MSEN: [u32; PWM_CHANNELS] = [
    PWM_CTL_MSEN1, PWM_CTL_MSEN2,
];

pub const PWM_STA_GAPO: [u32; PWM_CHANNELS] = [
    PWM_STA_GAPO1, PWM_STA_GAPO2,
];

pub const PWM_STA_STA: [u32; PWM_CHANNELS] = [
    PWM_STA_STA1, PWM_STA_STA2,
];

pub const SLV_DR_DATA_SHIFT: u8 = 0;
pub const SLV_DR_DATA_MASK: u32 = 0x000000ff;
pub const SLV_DR_OE:        u32 = 0x00000100;
pub const SLV_DR_UE:        u32 = 0x00000200;
pub const SLV_DR_TXBUSY:    u32 = 0x00010000;
pub const SLV_DR_RXFE:      u32 = 0x00020000;
pub const SLV_DR_TXFF:      u32 = 0x00040000;
pub const SLV_DR_RXFF:      u32 = 0x00080000;
pub const SLV_DR_TXFE:      u32 = 0x00100000;
pub const SLV_DR_RXBUSY:    u32 = 0x00200000;
pub const SLV_DR_TXFL_SHIFT: u8 = 22;
pub const SLV_DR_TXFL_MASK: u32 = 0x07c00000;
pub const SLV_DR_RXFL_SHIFT: u8 = 27;
pub const SLV_DR_RXFL_MASK: u32 = 0xf8000000;

pub const SLV_RSR_OE: u32 = 0x00000001;
pub const SLV_RSR_UE: u32 = 0x00000002;

pub const SLV_SLV_MASK: u32 = 0x0000007f;

pub const SLV_CR_EN:         u32 = 0x00000001;
pub const SLV_CR_SPI:        u32 = 0x00000002;
pub const SLV_CR_I2C:        u32 = 0x00000004;
pub const SLV_CR_CPHA:       u32 = 0x00000008;
pub const SLV_CR_CPOL:       u32 = 0x00000010;
pub const SLV_CR_ENSTAT:     u32 = 0x00000020;
pub const SLV_CR_ENCTRL:     u32 = 0x00000040;
pub const SLV_CR_BRK:        u32 = 0x00000080;
pub const SLV_CR_TXE:        u32 = 0x00000100;
pub const SLV_CR_RXE:        u32 = 0x00000200;
pub const SLV_CR_INV_RXF:    u32 = 0x00000400;
pub const SLV_CR_TESTFIFO:   u32 = 0x00000800;
pub const SLV_CR_HOSTCTRLEN: u32 = 0x00001000;
pub const SLV_CR_INV_TXF:    u32 = 0x00002000;

pub const SLV_FR_TXBUSY:    u32 = 0x00000001;
pub const SLV_FR_RXFE:      u32 = 0x00000002;
pub const SLV_FR_TXFF:      u32 = 0x00000004;
pub const SLV_FR_RXFF:      u32 = 0x00000008;
pub const SLV_FR_TXFE:      u32 = 0x00000010;
pub const SLV_FR_RXBUSY:    u32 = 0x00000020;
pub const SLV_FR_TXFL_SHIFT: u8 = 6;
pub const SLV_FR_TXFL_MASK: u32 = 0x000007c0;
pub const SLV_FR_RXFL_SHIFT: u8 = 11;
pub const SLV_FR_RXFL_MASK: u32 = 0x0000f800;

pub const SLV_IFLS_TX_MASK: u32 = 0x00000007;
pub const SLV_IFLS_TX_1_8:  u32 = 0x00000000;
pub const SLV_IFLS_TX_1_4:  u32 = 0x00000001;
pub const SLV_IFLS_TX_1_2:  u32 = 0x00000002;
pub const SLV_IFLS_TX_3_4:  u32 = 0x00000003;
pub const SLV_IFLS_TX_7_8:  u32 = 0x00000004;
pub const SLV_IFLS_RX_MASK: u32 = 0x00000038;
pub const SLV_IFLS_RX_1_8:  u32 = 0x00000000;
pub const SLV_IFLS_RX_1_4:  u32 = 0x00000008;
pub const SLV_IFLS_RX_1_2:  u32 = 0x00000010;
pub const SLV_IFLS_RX_3_4:  u32 = 0x00000018;
pub const SLV_IFLS_RX_7_8:  u32 = 0x00000020;

pub const SLV_IR_RX: u32 = 0x00000001;
pub const SLV_IR_TX: u32 = 0x00000002;
pub const SLV_IR_BE: u32 = 0x00000004;
pub const SLV_IR_OE: u32 = 0x00000008;

pub const SLV_TDR_MASK: u32 = 0x000000ff;

pub const SLV_GPUSTAT_MASK: u32 = 0x0000000f;

pub const SLV_HCTRL_MASK: u32 = 0x000000ff;

pub const SLV_DEBUG1_MASK: u32 = 0x03ffffff;

pub const SLV_DEBUG2_MASK: u32 = 0x00ffffff;

pub const AUX_IRQ_MU:   u32 = 0x00000001;
pub const AUX_IRQ_SPI1: u32 = 0x00000002;
pub const AUX_IRQ_SPI2: u32 = 0x00000004;

pub const AUX_ENB_MU:   u32 = 0x00000001;
pub const AUX_ENB_SPI1: u32 = 0x00000002;
pub const AUX_ENB_SPI2: u32 = 0x00000004;

pub const AUX_MU_IO_MASK: u32 = 0x000000ff;

pub const AUX_MU_IIR_RX: u32 = 0x00000001;
pub const AUX_MU_IIR_TX: u32 = 0x00000002;

pub const AUX_MU_IER_RX: u32 = 0x00000002;
pub const AUX_MU_IER_TX: u32 = 0x00000003;

pub const AUX_MU_LCR_8BIT:  u32 = 0x00000001;
pub const AUX_MU_LCR_BREAK: u32 = 0x00000040;
pub const AUX_MU_LCR_DLAB:  u32 = 0x00000080;

pub const AUX_MU_MCR_RTS: u32 = 0x00000002;

pub const AUX_MU_LSR_READY:  u32 = 0x00000001;
pub const AUX_MU_LSR_OVERRUN: u32 = 0x00000002;
pub const AUX_MU_LSR_TXEMPTY: u32 = 0x00000020;
pub const AUX_MU_LSR_TXIDLE:  u32 = 0x00000040;

pub const AUX_MU_MSR_CTS: u32 = 0x00000020;

pub const AUX_MU_SCRATCH_MASK: u32 = 0x000000ff;

pub const AUX_MU_CNTL_RXEN:      u32 = 0x00000001;
pub const AUX_MU_CNTL_TXEN:      u32 = 0x00000002;
pub const AUX_MU_CNTL_RAFC:      u32 = 0x00000004;
pub const AUX_MU_CNTL_CAFC:      u32 = 0x00000008;
pub const AUX_MU_CNTL_RAFL_MASK: u32 = 0x00000030;
pub const AUX_MU_CNTL_RAFL1:     u32 = 0x00000020;
pub const AUX_MU_CNTL_RAFL2:     u32 = 0x00000010;
pub const AUX_MU_CNTL_RAFL3:     u32 = 0x00000000;
pub const AUX_MU_CNTL_RAFL4:     u32 = 0x00000030;
pub const AUX_MU_CNTL_RALI:      u32 = 0x00000040;
pub const AUX_MU_CNTL_CALI:      u32 = 0x00000080;

pub const AUX_MU_STAT_RXAVAIL:    u32 = 0x00000001;
pub const AUX_MU_STAT_TXAVAIL:    u32 = 0x00000002;
pub const AUX_MU_STAT_RXIDLE:     u32 = 0x00000004;
pub const AUX_MU_STAT_TXIDLE:     u32 = 0x00000008;
pub const AUX_MU_STAT_OVERRUN:    u32 = 0x00000010;
pub const AUX_MU_STAT_TXFULL:     u32 = 0x00000020;
pub const AUX_MU_STAT_RTS:        u32 = 0x00000040;
pub const AUX_MU_STAT_CTS:        u32 = 0x00000080;
pub const AUX_MU_STAT_TXEMPTY:    u32 = 0x00000100;
pub const AUX_MU_STAT_TXDONE:     u32 = 0x00000200;
pub const AUX_MU_STAT_RXLEN_SHIFT: u8 = 16;
pub const AUX_MU_STAT_RXLEN_MASK: u32 = 0x00070000;
pub const AUX_MU_STAT_TXLEN_SHIFT: u8 = 24;
pub const AUX_MU_STAT_TXLEN_MASK: u32 = 0x07000000;

pub const AUX_MU_BAUD_MASK: u32 = 0x0000ffff;

pub const AUX_SPI_CNTL0_BITS_SHIFT: u8 = 0;
pub const AUX_SPI_CNTL0_BITS_MASK: u32 = 0x0000003f;
pub const AUX_SPI_CNTL0_MSBOUT:    u32 = 0x00000040;
pub const AUX_SPI_CNTL0_INVERTCLK: u32 = 0x00000080;
pub const AUX_SPI_CNTL0_TX_RISING: u32 = 0x00000100;
pub const AUX_SPI_CNTL0_CLRF:      u32 = 0x00000200;
pub const AUX_SPI_CNTL0_RX_RISING: u32 = 0x00000400;
pub const AUX_SPI_CNTL0_SPIEN:     u32 = 0x00000800;
pub const AUX_SPI_CNTL0_HOLD_MASK: u32 = 0x00003000;
pub const AUX_SPI_CNTL0_HOLD0:     u32 = 0x00000000;
pub const AUX_SPI_CNTL0_HOLD1:     u32 = 0x00001000;
pub const AUX_SPI_CNTL0_HOLD4:     u32 = 0x00002000;
pub const AUX_SPI_CNTL0_HOLD7:     u32 = 0x00003000;
pub const AUX_SPI_CNTL0_VARBIT:    u32 = 0x00004000;
pub const AUX_SPI_CNTL0_VARCS:     u32 = 0x00008000;
pub const AUX_SPI_CNTL0_POSTINPUT: u32 = 0x00010000;
pub const AUX_SPI_CNTL0_CS_SHIFT:   u8 = 17;
pub const AUX_SPI_CNTL0_CS_MASK:   u32 = 0x000e0000;
pub const AUX_SPI_CNTL0_SPD_SHIFT:  u8 = 20;
pub const AUX_SPI_CNTL0_SPD_MASK:  u32 = 0xfff00000;

pub const AUX_SPI_CNTL1_KEEP:      u32 = 0x00000001;
pub const AUX_SPI_CNTL1_MSBIN:     u32 = 0x00000002;
pub const AUX_SPI_CNTL1_IRQ_DONE:  u32 = 0x00000040;
pub const AUX_SPI_CNTL1_IRQ_EMPTY: u32 = 0x00000080;
pub const AUX_SPI_CNTL1_CSHT_SHIFT: u8 = 8;
pub const AUX_SPI_CNTL1_CSHT_MASK: u32 = 0x00000700;

pub const AUX_SPI_STAT_BITS_SHIFT: u8 = 0;
pub const AUX_SPI_STAT_BITS_MASK: u32 = 0x0000003f;
pub const AUX_SPI_STAT_BUSY:      u32 = 0x00000040;
pub const AUX_SPI_STAT_RXEMPTY:   u32 = 0x00000080;
pub const AUX_SPI_STAT_TXEMPTY:   u32 = 0x00000100;
pub const AUX_SPI_STAT_TXFULL:    u32 = 0x00000200;
pub const AUX_SPI_STAT_RXLEN_SHIFT: u8 = 16;
pub const AUX_SPI_STAT_RXLEN_MASK: u32 = 0x00ff0000;
pub const AUX_SPI_STAT_TXLEN_SHIFT: u8 = 24;
pub const AUX_SPI_STAT_TXLEN_MASK: u32 = 0xff000000;

pub const AUX_SPI_PEEK_MASK: u32 = 0x0000ffff;

pub const AUX_SPI_IO_MASK: u32 = 0x0000ffff;

pub const AUX_IRQ_SPI: [u32; AUX_SPI] = [
    AUX_IRQ_SPI1, AUX_IRQ_SPI2,
];

pub const AUX_ENB_SPI: [u32; AUX_SPI] = [
    AUX_ENB_SPI1, AUX_ENB_SPI2,
];
